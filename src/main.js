import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import firebase from 'firebase/app'
import '@/firebaseInit'
import { firestorePlugin } from 'vuefire'
import vuetify from './plugins/vuetify'

Vue.config.productionTip = false

Vue.use(firestorePlugin)

let app

const createApp = () => {
  if (!app) {
    new Vue({
      router,
      store,
      vuetify,
      render: h => h(App)
    }).$mount('#app')
  }
}

firebase.auth().onAuthStateChanged(user => {
  store.dispatch('bindUserData')
  createApp()
})

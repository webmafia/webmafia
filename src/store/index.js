import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router/index'
import { vuexfireMutations, firestoreAction } from 'vuexfire'
import firebase from 'firebase/app'
import { db, functions } from '@/firebaseInit'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    currentUser: null,
    dbUser: {},
    roomData: {
      id: '',
      roomTitle: '',
      state: '',
      master: {},
      timeCreated: null,
      playerList: [],
      roomMax: 0,
      roomMode: '',
      gameId: '',
      phaseLen: {
        nightLen: 0,
        dayLen: 0
      }
    },
    userRef: null,
    roomRef: null,
    gameData: {
      gameStarted: 0,
      survivorList: [],
      deathList: [],
      notices: [],
      phaseData: {
        phase: ''
      }
    },
    gamePrivateData: {
      job: ''
    }
  },
  mutations: {
    ...vuexfireMutations,
    updateRoomData: (state, roomData) => {
      state.roomData = roomData
    }
  },
  actions: {
    bindUserData: firestoreAction(async ({ state, dispatch, bindFirestoreRef, unbindFirestoreRef }) => {
      state.currentUser = firebase.auth().currentUser
      if (state.currentUser) {
        state.userRef = db.collection('uid_list').doc(state.currentUser.uid)
        dispatch('getUserTime')
        return bindFirestoreRef('dbUser', db.collection('uid_list').doc(state.currentUser.uid))
      } else {
        state.userRef = null
        unbindFirestoreRef('dbUser')
      }
    }),
    getUserTime: async ({ state }) => {
      await state.userRef.update({
        servertime: firebase.firestore.FieldValue.serverTimestamp()
      })
      const unsubscribe = state.userRef.onSnapshot(snapshot => {
        if (snapshot.data().servertime) {
          state.userTime = Date.now() - snapshot.data().servertime.toMillis()
          unsubscribe()
        }
      })
    },
    bindRoomData: firestoreAction(({ state, bindFirestoreRef }, { id }) => {
      state.roomRef = db.collection('lobby').doc(id)
      return bindFirestoreRef('roomData', db.collection('lobby').doc(id))
    }),
    leaveRoom: firestoreAction(async ({ state, unbindFirestoreRef }) => {
      try {
        await Promise.all([
          functions.httpsCallable('leaveRoom')({ id: state.roomRef.id }),
          unbindFirestoreRef('roomData')
        ])
        router.push('/lobby')
      } catch (e) {
        console.error(e)
      }
    }),
    bindGameData: firestoreAction(({ state, getters, bindFirestoreRef }) => {
      if (getters.gameRef === undefined || null) {
        console.error('gameRef is undefined || null')
        return
      }
      return Promise.all([
        bindFirestoreRef('gameData', getters.gameRef.collection('info').doc('public'), { wait: true }),
        bindFirestoreRef('gamePrivateData', getters.gameRef.collection('playerList').doc(state.currentUser.uid), { wait: true })
      ])
    }),
    unbindGameData: firestoreAction(({ unbindFirestoreRef }) => {
      unbindFirestoreRef('gameData', () => {
        return {
          gameStarted: 0,
          survivorList: [],
          deathList: [],
          notices: [],
          phaseData: {
            phase: ''
          }
        }
      })
      unbindFirestoreRef('gamePrivateData', () => {
        return {
          job: ''
        }
      })
    })
  },
  getters: {
    gameRef: state => {
      if (state.roomData.gameId) {
        return db.collection('game_log').doc(state.roomData.gameId)
      } else {
        return null
      }
    }
  },
  modules: {
  }
})

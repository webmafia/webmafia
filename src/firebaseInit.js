import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/functions'
import firebaseConfig from './firebaseConfig'
const firebaseApp = firebase.initializeApp(firebaseConfig)
const auth = firebaseApp.auth()
const db = firebaseApp.firestore()
// if (location.hostname === 'localhost') {
//   // Note that the Firebase Web SDK must connect to the WebChannel port
//   db.settings({
//     host: 'localhost:8080',
//     ssl: false
//   })
// }
const Timestamp = firebaseApp.firestore()
const functions = firebaseApp.functions('asia-northeast1')
// functions.useFunctionsEmulator('http://localhost:5001')
export { auth, db, functions, Timestamp }

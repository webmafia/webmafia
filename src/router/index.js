import Vue from 'vue'
import VueRouter from 'vue-router'

import store from '@/store/index'
import { db } from '@/firebaseInit'

import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Lobby from '../views/Lobby.vue'
import RoomList from '../components/RoomList.vue'
import Room from '../components/Room.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    beforeEnter: (to, from, next) => {
      if (store.state.currentUser === null) {
        next()
      } else {
        next('/')
      }
    }
  },
  {
    path: '/lobby',
    component: Lobby,
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: '',
        name: 'lobby',
        component: RoomList
      },
      {
        path: ':id',
        component: Room,
        beforeEnter: async (to, from, next) => {
          const doc = await db.collection('lobby').doc(to.params.id).get()
          if (!doc.exists) {
            alert('방이 존재하지 않습니다.')
            next('/lobby')
          } else {
            store.dispatch('bindRoomData', { id: to.params.id })
            next()
          }
        }
      }
    ]
  },
  {
    path: '*',
    redirect: '/'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const currentUser = store.state.currentUser
  if (to.matched.some(record => record.meta.requiresAuth) && currentUser === null) {
    next({ path: '/login' })
  } else {
    next()
  }
})

export default router

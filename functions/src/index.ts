import * as functions from 'firebase-functions'
import admin = require('firebase-admin')
admin.initializeApp()
// admin.initializeApp({
//   credential: admin.credential.cert(require('../../key/webmafia-online-admin.json'))
// })
const db = admin.firestore()

export const leaveRoom = functions.region('asia-northeast1').https.onCall(async (data, context) => {
  if (!context.auth) {
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
        'while authenticated.')
  }
  const uid = context.auth.uid
  const userRef = db.collection('uid_list').doc(uid)
  const roomRef = db.collection('lobby').doc(data.id)
  try {
    return db.runTransaction(async (t: any) => {
      const [roomDoc] = await Promise.all([t.get(roomRef), userRef.update({ room: null })])
      if (!roomDoc.exists) {
        return t.delete(roomRef)
      }

      const playerList = roomDoc.data().playerList

      if (playerList.length === 1 && playerList[0].uid === uid) {
        t.delete(roomRef)
      } else {
        interface player {
          uid: string
        }
        const index = playerList.findIndex((x: player) => x.uid === uid)
        if (index >= 0) {
          playerList.splice(index, 1)
          if (roomDoc.data().master.uid === uid) {
            t.update(roomRef, {
              master: playerList[0]
            })
          }
          t.update(roomRef, {
            playerList: playerList
          })
        } else {
          t.update(roomRef, {})
        }
      }
    })
  } catch (e) {
    throw new functions.https.HttpsError(e.code, e.message, e.details)
  }
})

export const roomReady = functions.region('asia-northeast1').https.onCall(async (data, context) => {
  if (!context.auth) {
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
        'while authenticated.')
  }
  const uid = context.auth.uid
  const roomRef = db.collection('lobby').doc(data.id)
  try {
    return db.runTransaction(async (t: any) => {
      const doc = await t.get(roomRef)
        if (doc.exists) {
          const playerList = doc.data().playerList
          interface player {
            uid: string
          }
          const index = playerList.findIndex((x: player) => x.uid === uid)
          if (index !== -1) {
            playerList[index].ready = !playerList[index].ready
            t.update(roomRef, { playerList: playerList })
          }
        }
    })
  } catch (e) {
    throw new functions.https.HttpsError(e.code, e.message, e.details)
  }
})

type MafiaJob = 'mafia' | 'police' | 'doctor' | 'citizen'
interface Player {
  uid: string,
  displayName: string,
  ready?: boolean,
  job?: MafiaJob
}

type Phase = 'nightTime' | 'dayTime' | 'firstVoteTime' | 'secondVoteTime' | 'firstFinalArgTime' | 'secondFinalArgTime' | 'firstFinalVoteTime' | 'secondFinalVoteTime' | 'sunFallTime' | 'gameOver'

interface PhaseLen {
  nightLen: number,
  dayLen: number,
  voteLen: number,
  finalArgLen: number,
  finalVoteLen: number,
  sunFallLen: number
}
interface JobNum {
  mafia: number,
  police: number,
  doctor: number,
  citizen: number
}

interface RoomData {
  playerList: Player[],
  phaseLen: PhaseLen,
  jobNum: JobNum,
  master: Player
}

interface PhaseData {
  phase: Phase,
  phaseDate: number,
  phaseSecond : number,
  phaseName : '밤' | '낮' | '투표' | '재투표' | '최후변론' | '최종투표' | '해질녘' | '게임종료',
  phaseStarted : admin.firestore.Timestamp,
  suspect?: Player
}

interface GameData {
  mode: 'mafia',
  roomID: string,
  timeStarted: admin.firestore.Timestamp,
  phaseLen: PhaseLen,
  jobNum: JobNum
}

interface AdminData {
  timeStarted: admin.firestore.Timestamp,
  survivorList: Player[],
  deathList : Player[],
  notices: Notice[],
  jobList: {
    [key: string]: Player[]
  },
  nightLog: Night[],
  voteLog: VoteBox[],
  phaseData: PhaseData
}

interface Night {
  date: number,
  actionList: Action[]
}

interface Action {
  actionName: 'kill' | 'heal' | 'investigate',
  target: Player,
  user: Player
}

interface VoteBox {
  date: number,
  phase: Phase,
  voteList: Vote[]
}

interface Vote {
  user: Player,
  target: Player
}

interface Voted extends Player {
  count: number
}

interface Notice {
  date: number,
  phase: Phase,
  suspect?: Player
}

interface NoticeMsg {
  uid: 'admin',
  displayName: '관리자',
  msg: string,
  timeSent: admin.firestore.Timestamp
}

function shuffle<T>(a: Array<T>): Array<T> {
  for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1))
      ;[a[i], a[j]] = [a[j], a[i]]
  }
  return a
}

function jobShuffle(len: number, jobNum: JobNum): MafiaJob[] {
  const jobArray: MafiaJob[] = []
  Object.entries(jobNum).forEach(([key, value]) => {
    jobArray.push(<MafiaJob>key)
  })
  while (len > jobArray.length) {
    jobArray.push('citizen')
  }
  return shuffle(jobArray)
}

export const gameStart = functions.region('asia-northeast1').https.onCall(async (data, context) => {
  if (!context.auth) {
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
        'while authenticated.')
  }
  const uid = context.auth.uid
  const roomRef = db.collection('lobby').doc(data.id)

  try {
    return db.runTransaction(async (t: any) => {
      const roomDoc = await t.get(roomRef)
      if (!roomDoc.exists) {
        throw new functions.https.HttpsError('invalid-argument', 'The room does not exists.')
      }

      const { playerList, phaseLen, jobNum, master }: RoomData = roomDoc.data()

      if (master.uid !== uid) {
        throw new functions.https.HttpsError('permission-denied', 'You are not master of this room.')
      }

      const {
        nightLen,
        dayLen,
        voteLen,
        finalArgLen,
        finalVoteLen,
        sunFallLen
      }: PhaseLen = phaseLen

      if ((10 <= nightLen && nightLen <= 30 &&
           10 <= dayLen && dayLen <= 60 &&
           10 <= voteLen && voteLen <= 30 &&
           10 <= finalArgLen && finalArgLen <= 30 &&
           10 <= finalVoteLen && finalVoteLen <= 30 &&
           sunFallLen <= 10) === false) {
        throw new functions.https.HttpsError('failed-precondition', 'Phase Length is not appropriate.')
      }

      if (playerList.findIndex((e: Player) => !e.ready) !== -1) {
        throw new functions.https.HttpsError('failed-precondition', 'Not all players are ready.')
      }
      
      // if (playerList.length < 3) return '플레이어 숫자가 부족합니다.'

      if (false && Object.entries(jobNum).reduce((total, [key, value]) => total + value, 0) > playerList.length) return '플레이어 숫자가 부족합니다.'

      const gameRef = db.collection('game_log').doc()
      const timestamp: admin.firestore.Timestamp = admin.firestore.Timestamp.now()
      const jobArray = jobShuffle(playerList.length, jobNum)

      const survivorList = playerList
      const tempJobArray = [...jobArray]
      const jobList: {
        [key: string]: Player[]
      } = {
        mafia: [],
        doctor: [],
        police: [],
        citizen: []
      }
      survivorList.forEach((survivor: Player) => {
        const job = tempJobArray.shift()
        if (job !== undefined) {
          survivor.job = job
          jobList[job].push(survivor)
        }
      })

      const phaseData: PhaseData = {
        phase: 'nightTime',
        phaseDate: 1,
        phaseSecond: nightLen,
        phaseName: '밤',
        phaseStarted: timestamp
      }

      await Promise.all([
        gameRef.set({
          mode: 'mafia',
          roomID: roomRef.id,
          timeStarted: timestamp,
          phaseLen,
          jobNum
        }),
        gameRef.collection('chatList').add({
          uid: 'admin',
          displayName: '관리자',
          msg: '게임이 시작되었습니다.',
          timeSent: admin.firestore.FieldValue.serverTimestamp()
        }),
        gameRef.collection('info').doc('admin').set({
          timeStarted: timestamp,
          survivorList: survivorList,
          deathList : [],
          notices: [],
          jobList: jobList,
          nightLog: [],
          voteLog: [],
          // publicVoteList: [],
          phaseData: phaseData
        }),
        gameRef.collection('info').doc('public').set({
          gameStarted: timestamp,
          survivorList: survivorList,
          deathList: [],
          notices: [],
          phaseData: phaseData
        })
      ])

      const batch = db.batch()
      for (let i = 0; i < survivorList.length; i++) {
        const gameUserRef = gameRef.collection('playerList').doc(survivorList[i].uid)
        batch.set(gameUserRef, {
          job: jobArray[i],
          privateNotices: []
        })
      }
      jobList.mafia.map(player => {
        batch.set(gameRef.collection('mafiaChatList').doc(player.uid), {
          messages: []
        })
      })
      await Promise.all([
        batch.commit(),
        t.update(roomRef, {
          state: '게임중',
          gameId: gameRef.id
        })
      ])
      return 'The game started.'
    })
  } catch (e) {
    throw new functions.https.HttpsError(e.code, e.message, e.details)
  }
})


function openVoteBox (voteResult: Voted[], voteLog: VoteBox[], beforePhase: PhaseData): Voted[] {
  const currentVoteBox: VoteBox = voteLog.find(voteBox => 
    voteBox.date === beforePhase.phaseDate &&
    voteBox.phase === beforePhase.phase
  ) || {
    date: beforePhase.phaseDate,
    phase: beforePhase.phase,
    voteList: []
  }
  voteResult.forEach(voted => {
    voted.count = currentVoteBox.voteList.filter(vote => vote.target.uid === voted.uid).length
  })
  voteResult.sort((a, b) => b.count - a.count)
  return voteResult
}

export const phaseStart = functions.region('asia-northeast1').https.onCall(async (data, context) => {
  if (!context.auth) {
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
        'while authenticated.')
  }
  const gameRef = db.collection('game_log').doc(data.gameId)
  const publicRef = gameRef.collection('info').doc('public')
  const adminRef = gameRef.collection('info').doc('admin')

  try {
    return db.runTransaction(async (t: any) => {
      const [gameDoc, publicDoc, adminDoc] = await Promise.all([
        t.get(gameRef), t.get(publicRef), t.get(adminRef)
      ])

      if (!(gameDoc.exists && publicDoc.exists && adminDoc.exists)) {
        throw new functions.https.HttpsError('invalid-argument', 'The game does not exists.')
      }
      const {
        phaseLen,
        roomID
      }: GameData = gameDoc.data()

      const {
        survivorList,
        deathList,
        notices,
        // jobList,
        nightLog,
        voteLog,
        phaseData
      }: AdminData = adminDoc.data()
      const timestamp = admin.firestore.Timestamp.now()
      const requestTime = phaseData.phaseStarted.toMillis() + phaseData.phaseSecond * 1000
      const currentTime = timestamp.toMillis()
      if (requestTime > currentTime) {
        throw new functions.https.HttpsError('out-of-range', 'Request came too early.')
      }
      if (notices.find(notice => notice.date === phaseData.phaseDate && notice.phase === phaseData.phase)) {
        throw new functions.https.HttpsError('already-exists', 'Requested phase is already calculated.')
      }
      if (phaseData.phase === 'gameOver') {
        throw new functions.https.HttpsError('failed-precondition', 'The game is already finished')
      }

      const beforePhase: PhaseData = phaseData
      let newPhase: PhaseData = phaseData
      let voteResult: Voted[] = []
      let beforeNotice: Notice | undefined
      const newNotice: Notice = {
        date: beforePhase.phaseDate,
        phase: beforePhase.phase
      }
      const noticeMsg: NoticeMsg = {
        uid: 'admin',
        displayName: '관리자',
        msg: '',
        timeSent: timestamp
      }
      let newSurvivorList: Player[] = survivorList
      
      switch (beforePhase.phase) {
        case 'nightTime':
          const currentNight: Night = nightLog.find(night => night.date === beforePhase.phaseDate) || nightLog[nightLog.push({
            date: beforePhase.phaseDate,
            actionList: []
          }) - 1]
          const killAction = currentNight.actionList.find(action => action.actionName === 'kill')
          const healAction = currentNight.actionList.find(action => action.actionName === 'heal')
          if (killAction === undefined) {
            noticeMsg.msg = `${beforePhase.phaseDate}번째 ${beforePhase.phaseName}에는 아무 일도 일어나지 않았습니다.`
          } else if (healAction === undefined || healAction.target.uid !== killAction.target.uid) {
            noticeMsg.msg = `${beforePhase.phaseDate}번째 ${beforePhase.phaseName}에 ${killAction.target.displayName}님이 사망하셨습니다.`
            newSurvivorList = survivorList.filter(survivor => survivor.uid !== killAction.target.uid)
            deathList.push(killAction.target)
          } else {
            noticeMsg.msg = `${beforePhase.phaseDate}번째 ${beforePhase.phaseName}에 화타가 뛰어난 의술로 죽을 뻔한 누군가를 살려냈습니다!`
          }

          newPhase = {
            phase: 'dayTime',
            phaseSecond: phaseLen.dayLen,
            phaseName: '낮',
            phaseDate: beforePhase.phaseDate,
            phaseStarted: timestamp
          }
          break
        case 'dayTime':
          noticeMsg.msg = `투표 시간입니다.`
          newPhase = {
            phase: 'firstVoteTime',
            phaseSecond: phaseLen.voteLen,
            phaseName: '투표',
            phaseDate: beforePhase.phaseDate,
            phaseStarted: timestamp
          }
          break
        case 'firstVoteTime':
          voteResult = survivorList.map(survivor => {
            (<Voted> survivor).count = 0
            return <Voted> survivor
          })
          voteResult.unshift({
            uid: 'skip',
            displayName: '무투표',
            count: 0
          })
          voteResult = openVoteBox(voteResult, voteLog, beforePhase)
          // developing - modified required
          noticeMsg.msg = `투표결과: \n`
          voteResult.forEach(vote => {
            noticeMsg.msg += `${vote.displayName}: ${vote.count}표\n`
          })
          if (voteResult[0].uid === 'skip') {
            noticeMsg.msg += `무투표가 최다 득표를 하여 곧 밤이 됩니다. 집으로 돌아가 잘 준비를 해주세요.`
            newPhase = {
              phase: 'sunFallTime',
              phaseSecond: phaseLen.sunFallLen,
              phaseName: '해질녘',
              phaseDate: beforePhase.phaseDate,
              phaseStarted: timestamp
            }
          } else if (voteResult.length === 2 || voteResult[0].count !== voteResult[1].count) {
            noticeMsg.msg += `${voteResult[0].displayName}님이 ${voteResult[0].count}표를 받아 최후의 변론을 진행합니다.`
            newNotice.suspect = voteResult[0]
            newPhase = {
              phase: 'firstFinalArgTime',
              phaseSecond: phaseLen.finalArgLen,
              phaseName: '최후변론',
              phaseDate: beforePhase.phaseDate,
              phaseStarted: timestamp
            }
          } else {
            noticeMsg.msg += `최다 득표자가 여러 명 발생하여 곧 밤이 됩니다. 집으로 돌아가 잘 준비를 해주세요.`
            newPhase = {
              phase: 'sunFallTime',
              phaseSecond: phaseLen.sunFallLen,
              phaseName: '해질녘',
              phaseDate: beforePhase.phaseDate,
              phaseStarted: timestamp
            }
          }
          break
        case 'secondVoteTime':
          voteResult = survivorList.map(survivor => {
            (<Voted> survivor).count = 0
            return <Voted> survivor
          })
          voteResult.unshift({
            uid: 'skip',
            displayName: '무투표',
            count: 0
          })
          voteResult = openVoteBox(voteResult, voteLog, beforePhase)
          // developing - modified required
          noticeMsg.msg = `투표결과: \n`
          voteResult.forEach(vote => {
            noticeMsg.msg += `${vote.displayName}: ${vote.count}표\n`
          })
          if (voteResult[0].uid === 'skip') {
            noticeMsg.msg += `무투표가 최다 득표를 하여 곧 밤이 됩니다. 집으로 돌아가 잘 준비를 해주세요.`
            newPhase = {
              phase: 'sunFallTime',
              phaseSecond: phaseLen.sunFallLen,
              phaseName: '해질녘',
              phaseDate: beforePhase.phaseDate,
              phaseStarted: timestamp
            }
          } else if (voteResult.length === 2 || voteResult[0].count !== voteResult[1].count) {
            noticeMsg.msg += `${voteResult[0].displayName}님이 ${voteResult[0].count}표를 받아 최후의 변론을 진행합니다.`
            newNotice.suspect = voteResult[0]
            newPhase = {
              phase: 'secondFinalArgTime',
              phaseSecond: phaseLen.finalArgLen,
              phaseName: '최후변론',
              phaseDate: beforePhase.phaseDate,
              phaseStarted: timestamp
            }
          } else {
            noticeMsg.msg += `재투표 결과 최다 득표자가 여러 명 발생하여 곧 밤이 됩니다. 집으로 돌아가 잘 준비를 해주세요.`
            newPhase = {
              phase: 'sunFallTime',
              phaseSecond: phaseLen.sunFallLen,
              phaseName: '해질녘',
              phaseDate: beforePhase.phaseDate,
              phaseStarted: timestamp
            }
          }
          break
        case 'firstFinalArgTime':
          beforeNotice = notices.find(notice => notice.date === beforePhase.phaseDate && notice.phase === 'firstVoteTime')
          if (beforeNotice === undefined || beforeNotice.suspect === undefined) throw new functions.https.HttpsError('failed-precondition', 'suspect is missing.')
          newNotice.suspect = beforeNotice.suspect
          noticeMsg.msg = `${newNotice.suspect?.displayName}님의 최종 투표 시간입니다.`
          newPhase = {
            phase: 'firstFinalVoteTime',
            phaseSecond: phaseLen.finalVoteLen,
            phaseName: '최종투표',
            phaseDate: beforePhase.phaseDate,
            phaseStarted: timestamp
          }
          break
        case 'secondFinalArgTime':
          beforeNotice = notices.find(notice => notice.date === beforePhase.phaseDate && notice.phase === 'secondVoteTime')
          if (beforeNotice === undefined || beforeNotice.suspect === undefined) throw new functions.https.HttpsError('failed-precondition', 'suspect is missing.')
          newNotice.suspect = beforeNotice.suspect
          noticeMsg.msg = `${newNotice.suspect?.displayName}님의 최종 투표 시간입니다.`
          newPhase = {
            phase: 'secondFinalVoteTime',
            phaseSecond: phaseLen.finalVoteLen,
            phaseName: '최종투표',
            phaseDate: beforePhase.phaseDate,
            phaseStarted: timestamp
          }
          break
        case 'firstFinalVoteTime':
          beforeNotice = notices.find(notice => notice.date === beforePhase.phaseDate && notice.phase === 'firstFinalArgTime')
          if (beforeNotice === undefined || beforeNotice.suspect === undefined) throw new functions.https.HttpsError('failed-precondition', 'suspect is missing.')
          newNotice.suspect = beforeNotice.suspect
          voteResult = [{
            uid: 'down',
            displayName: '찬성',
            count: 0
          },
          {
            uid: 'up',
            displayName: '반대',
            count: 0
          }]
          voteResult = openVoteBox(voteResult, voteLog, beforePhase)
          // developing - modified required
          noticeMsg.msg = `투표결과: \n`
          voteResult.forEach(vote => {
            noticeMsg.msg += `${vote.displayName}: ${vote.count}표\n`
          })
          if (voteResult[0].count !== voteResult[1].count) {
            if (voteResult[0].uid === 'down') {
              noticeMsg.msg += `${newNotice.suspect?.displayName}님이 형장의 이슬로 사라졌습니다. `
              newSurvivorList = survivorList.filter(survivor => survivor.uid !== newNotice.suspect?.uid)
              deathList.push(newNotice.suspect)
            } else {
              noticeMsg.msg += `${newNotice.suspect?.displayName}님이 단두대에서 내려왔습니다. `
            }
            noticeMsg.msg += `곧 밤이 됩니다. 집으로 돌아가 잘 준비를 해주세요.`
            newPhase = {
              phase: 'sunFallTime',
              phaseSecond: phaseLen.sunFallLen,
              phaseName: '해질녘',
              phaseDate: beforePhase.phaseDate,
              phaseStarted: timestamp
            }
          } else {
            noticeMsg.msg += `찬성과 반대가 같은 표를 받아 재투표를 진행합니다.`
            newPhase = {
              phase: 'secondVoteTime',
              phaseSecond: phaseLen.finalArgLen,
              phaseName: '재투표',
              phaseDate: beforePhase.phaseDate,
              phaseStarted: timestamp
            }
          }
          break
        case 'secondFinalVoteTime':
          beforeNotice = notices.find(notice => notice.date === beforePhase.phaseDate && notice.phase === 'secondFinalArgTime')
          if (beforeNotice === undefined || beforeNotice.suspect === undefined) throw new functions.https.HttpsError('failed-precondition', 'suspect is missing.')
          newNotice.suspect = beforeNotice.suspect
          voteResult = [{
            uid: 'down',
            displayName: '찬성',
            count: 0
          },
          {
            uid: 'up',
            displayName: '반대',
            count: 0
          }]
          voteResult = openVoteBox(voteResult, voteLog, beforePhase)
          // developing - modified required
          noticeMsg.msg = `투표결과: \n`
          voteResult.forEach(vote => {
            noticeMsg.msg += `${vote.displayName}: ${vote.count}표\n`
          })
          if (voteResult[0].count !== voteResult[1].count) {
            if (voteResult[0].uid === 'down') {
              noticeMsg.msg = `${newNotice.suspect?.displayName}님이 형장의 이슬로 사라졌습니다. `
              newSurvivorList = survivorList.filter(survivor => survivor.uid !== newNotice.suspect?.uid)
              deathList.push(newNotice.suspect)
            } else {
              noticeMsg.msg = `${newNotice.suspect?.displayName}님이 단두대에서 내려왔습니다. `
            }
            noticeMsg.msg += `곧 밤이 됩니다. 집으로 돌아가 잘 준비를 해주세요.`
            newPhase = {
              phase: 'sunFallTime',
              phaseSecond: phaseLen.sunFallLen,
              phaseName: '해질녘',
              phaseDate: beforePhase.phaseDate,
              phaseStarted: timestamp
            }
          } else {
            noticeMsg.msg = `찬성 및 반대가 같은 표를 받아 곧 밤이 됩니다. 집으로 돌아가 잘 준비를 해주세요.`
            newPhase = {
              phase: 'sunFallTime',
              phaseSecond: phaseLen.sunFallLen,
              phaseName: '해질녘',
              phaseDate: beforePhase.phaseDate,
              phaseStarted: timestamp
            }
          }
          break
        case 'sunFallTime':
          noticeMsg.msg = `밤이 되었습니다.`
          newPhase = {
            phase: 'nightTime',
            phaseSecond: phaseLen.nightLen,
            phaseName: '밤',
            phaseDate: beforePhase.phaseDate + 1,
            phaseStarted: timestamp
          }
          break
      }
      
      if (newNotice.suspect !== undefined) {
        newPhase.suspect = newNotice.suspect
      }

      const mafiaCount = newSurvivorList.reduce((prev, value) => {
        if (value.job === 'mafia') {
          return prev + 1
        } else {
          return prev
        }
      }, 0)
      if (mafiaCount * 2 >= newSurvivorList.length) {
        noticeMsg.msg += `\n마피아 팀의 승리로 게임이 종료되었습니다.`
        newPhase = {
          ...newPhase,
          phase: 'gameOver',
          phaseSecond: 0,
          phaseName: '게임종료'
        }
        t.update(db.collection('lobby').doc(roomID), {
          state: '준비중'
        })
      } else if (mafiaCount === 0) {
        noticeMsg.msg += `\n시민 팀의 승리로 게임이 종료되었습니다.`
        newPhase = {
          ...newPhase,
          phase: 'gameOver',
          phaseSecond: 0,
          phaseName: '게임종료',
          phaseStarted: admin.firestore.Timestamp.now()
        }
        t.update(db.collection('lobby').doc(roomID), {
          state: '준비중'
        })
      }
      
      t.update(publicRef, {
        survivorList: newSurvivorList,
        deathList,
        notices: admin.firestore.FieldValue.arrayUnion(newNotice),
        phaseData: newPhase
      })

      t.update(adminRef, {
        survivorList: newSurvivorList,
        deathList,
        notices: admin.firestore.FieldValue.arrayUnion(newNotice),
        phaseData: newPhase
      })

      return noticeMsg
    }).then(async notice => {
      await gameRef.collection('chatList').add(notice)
      return `New phase started. ${notice.msg}`
    })
  } catch (e) {
    throw new functions.https.HttpsError(e.code, e.message, e.details)
  }
})

export const nightAction = functions.region('asia-northeast1').https.onCall(async (data, context) => {
  if (!context.auth) {
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
        'while authenticated.')
  }
  const uid = context.auth.uid
  const displayName: string = context.auth.token.name
  // target should be verified
  const target: Player = data.target
  const gameRef = db.collection('game_log').doc(data.gameId)
  const adminRef = gameRef.collection('info').doc('admin')
  const gameUserRef = gameRef.collection('playerList').doc(uid)

  try {
    return db.runTransaction(async (t: any) => {
      const [gameDoc, adminDoc] = await Promise.all([
        t.get(gameRef), t.get(adminRef), t.get(gameUserRef)
      ])
      if (!(gameDoc.exists && adminDoc.exists)) {
        throw new functions.https.HttpsError('invalid-argument', 'The game does not exists.')
      }

      const {
        survivorList,
        // deathList,
        // notices,
        jobList,
        nightLog,
        // voteLog,
        phaseData
      }: AdminData = adminDoc.data()

      if (phaseData.phase !== 'nightTime') {
        throw new functions.https.HttpsError('failed-precondition', 'Current Phase is not night.')
      }

      if (survivorList.findIndex(survivor => survivor.uid === uid) < 0) {
        throw new functions.https.HttpsError('permission-denied', 'You are not an alive player.')
      }

      const job = Object.keys(jobList).find((key => jobList[key].find((player: Player) => player.uid === uid)))
      
      const tonight: Night = nightLog.find(night => night.date === phaseData.phaseDate) || nightLog[nightLog.push({
        date: phaseData.phaseDate,
        actionList: []
      }) - 1]
      
      const privateNotice = {
        date: phaseData.phaseDate,
        phase: phaseData.phase
      }
      const noticeMsg = {
        uid: 'admin',
        displayName: '관리자',
        msg: ``,
        timeSent: admin.firestore.Timestamp.now()
      }
      switch (job) {
        case 'mafia':
          tonight.actionList = tonight.actionList.filter(action => action.actionName !== 'kill')
          tonight.actionList.push({
            actionName: 'kill',
            target: target,
            user: {
              uid: uid,
              displayName: displayName
            }
          })
          t.update(adminRef, { nightLog: nightLog })
          noticeMsg.msg = `${target.displayName}님을 죽였습니다.`
          break
        case 'doctor':
          tonight.actionList = tonight.actionList.filter(action => action.user.uid !== uid)
          tonight.actionList.push({
            actionName: 'heal',
            target: target,
            user: {
              uid: uid,
              displayName: displayName
            }
          })
          t.update(adminRef, { nightLog: nightLog })
          noticeMsg.msg = `${target.displayName}님을 살렸습니다.`
          break
        case 'police':
          const index = tonight.actionList.findIndex(action => action.user.uid === uid)
          if (index >= 0) {
            throw new functions.https.HttpsError('failed-precondition', 'You already used your action.')
          }
          tonight.actionList.push({
            actionName: 'investigate',
            target: target,
            user: {
              uid: uid,
              displayName: displayName
            }
          })
          const targetJob = Object.keys(jobList).find(key => jobList[key].find((player: Player) => player.uid === target.uid))
          t.update(adminRef, { nightLog: nightLog })
          if (targetJob === 'mafia') {
            noticeMsg.msg = `${target.displayName}님은 마피아가 맞습니다.`
          } else {
            noticeMsg.msg = `${target.displayName}님은 마피아가 아닙니다.`
          }
          break
        default:
          break
      }
      t.update(gameUserRef, {
        privateNotices: admin.firestore.FieldValue.arrayUnion(privateNotice)
      })
      return noticeMsg
    }).then(async notice => {
      await gameUserRef.collection('messages').add(notice)
      return notice.msg
    })
  } catch (e) {
    throw new functions.https.HttpsError(e.code, e.message, e.details)
  }
})

function suspectValidityCheck(target: Player, uid: string, phaseData: PhaseData) {
  if (phaseData.suspect === undefined) {
    throw new functions.https.HttpsError('failed-precondition', 'Phase data is wrong.')
  }
  if (phaseData.suspect.uid === uid) {
    throw new functions.https.HttpsError('permission-denied', 'You are not a qualified voter.')
  }
  if (target.uid !== 'up' && target.uid !== 'down') {
    throw new functions.https.HttpsError('invalid-argument', 'You should only vote up or down.')
  }
}

export const voteAction = functions.region('asia-northeast1').https.onCall(async (data, context) => {
  if (!context.auth) {
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
        'while authenticated.')
  }
  const uid = context.auth.uid
  const displayName: string = context.auth.token.name
  const gameRef = db.collection('game_log').doc(data.gameId)
  // target should be verified
  const target = data.target
  const adminRef = gameRef.collection('info').doc('admin')

  try {
    return db.runTransaction(async (t: any) => {
      const [gameDoc, adminDoc] = await Promise.all([
        t.get(gameRef), t.get(adminRef)
      ])

      if (!(gameDoc.exists && adminDoc.exists)) {
        throw new functions.https.HttpsError('invalid-argument', 'The game does not exists.')
      }

      const {
        survivorList,
        // deathList,
        // notices,
        // jobList,
        // nightLog,
        voteLog,
        phaseData
      }: AdminData = adminDoc.data()
      if (phaseData.phase.includes('Vote') === false) {
        throw new functions.https.HttpsError('failed-precondition', 'Current Phase is not voting time.')
      }

      if (survivorList.findIndex(survivor => survivor.uid === uid) < 0) {
        throw new functions.https.HttpsError('permission-denied', 'You are not an alive player.')
      }

      if (phaseData.phase.includes('FinalVote')) {
        suspectValidityCheck(target, uid, phaseData)
      }

      const currentVote: VoteBox = voteLog.find(voteBox => voteBox.date === phaseData.phaseDate && voteBox.phase === phaseData.phase) ||
      voteLog[voteLog.push({
        date: phaseData.phaseDate,
        phase: phaseData.phase,
        voteList: []
      }) - 1]
      currentVote.voteList = currentVote.voteList.filter(vote => vote.user.uid !== uid)
      currentVote.voteList.push({
        target: target,
        user: {
          uid: uid,
          displayName: displayName
        }
      })
      t.update(adminRef, { voteLog: voteLog })
      return `voted to ${target.displayName}.`
    })
  } catch (e) {
    throw new functions.https.HttpsError(e.code, e.message, e.details)
  }
})

export const userCreated = functions.region('asia-northeast1').auth.user().onCreate(async user => {
  try {
    let nickname = user.displayName
    if (user.displayName) {
      const doc = await db.collection('uid_list').where('nickname', '==', user.displayName).get()
      if (!doc.empty) {
        await admin.auth().updateUser(user.uid, {
          displayName: undefined
        })
        nickname = undefined
      }
    }
    return db.collection('uid_list').doc(user.uid).set({
      nickname: nickname,
      email: user.email,
      level: 1,
      level_exp: 0
    })
  } catch (e) {
    throw new functions.https.HttpsError(e.code, e.message, e.details)
  }
})

export const userDeleted = functions.region('asia-northeast1').auth.user().onDelete(user => {
  try {
    return db.collection('uid_list').doc(user.uid).delete()
  } catch (e) {
    throw new functions.https.HttpsError(e.code, e.message, e.details)
  }
})
